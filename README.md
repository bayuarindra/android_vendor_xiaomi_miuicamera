# MiuiCamera-Leica
## Getting Started :
### Cloning :
• Clone this repo in vendor/xiaomi/miuicamera in your working directory by :
```
git clone https://gitlab.com/bayuarindra/vendor_xiaomi_miuicamera.git -b tiramisu vendor/xiaomi/miuicamera
```
### Changes Required :
• You will need following changes in your device tree:

• Makefile changes
```
 $(call inherit-product-if-exists, vendor/xiaomi/miuicamera/miuicamera.mk)
 ```

• Props Changes
```
PRODUCT_PRODUCT_PROPERTIES += \
    ro.product.mod_device=codename_global
```

• Ship MiuiGallery & Editor
```
TARGET_SHIPS_GALLERY := true
```

• Ship MiuiScanner
```
TARGET_SHIPS_Scanner := true
```

• Done, continue building your ROM as you do normally.


## Credit:
• @hdzungx For inherit camera for POCO F4
• @youthkinga for MiuiGallery & Editor Port
•  @聖小熊、@lostark13、@itzdfplayer、@Sevtinge for LeicaCamMod
